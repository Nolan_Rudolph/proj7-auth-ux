Author: Nolan Rudolph  
DuckID: 951553198  
E-mail: ngr@uoregon.edu  
  
WELCOME TO MY FINAL PROJECT FOR CIS 322  
  
Boy did we cover a bunch of subjects in this project!  
Let me begin by saying someone should definitely use my website in a browser instead of from terminal.  
  
I have a lot of webpages now, so I'll break this down into sections.  
  
(login.html)  
Login.html is a page designed to say hello to the user, and allow them the option of registering or logging in.  
If a user decided to register, they will click on the "Register" button.  
  
(/app/register.html)  
Register.html allows the user to create a username and password, encryped upon submittion and protected by CSRF.  
This will then store the user in the database using a dictionary structure, where it can be referenced for a login.  
This page will then redirect you back to login.html, and a logical next step would be to login. Press "Login".  
  
(supaLogin.html)  
SupaLogin.html is the result of too many names being used for webpages. This is the ultimate login page!  
This page is responsible for seeing if your username is in the database (A), and then  
using the password you submit and comparing it to the decrypted password tied to the username in the database (B).  
If these match, then you are allowed access in to start submitting your brevet times. Now, if you neglected to  
check "Remember Me", you will be granted access to the software for 10 minutes, more than enough time to insert  
a race. However, if you feel you need some extra time or just don't want to log back in, make sure to check that  
box to acquire permanent authorization to the software.  
  
(setup.html)  
So you logged in! Now you can initialize the race parameters to get accurate results of brevet times.  
Just hit submit once all fields are filled out correctly, or if you have to leave, make sure to press that logout button.  
  
(index.html)  
Alrite so you've set up your race, time to start submitting times! A new addition added, due to the loss of  
points from a prior project, is I know have AJAX requests and handlers that will show you the open and closing times  
you are about to submit! This allows the user to make sure they want to add the brevet stop before continuing on  
and submitting it. Like setup.html, if you have to take a break, just click that logout button and be mindful  
of the 10 minutes of authorization you have to your token if you neglected to check "Remember Me".  
  
Once you have submitted all of your stops, you can review the stops you've added in many ways!  
Here are a few things you can do:  
1.) Simply get the times:  
- http://<host:port>/api/listAll // This allows you to view both opening and closing times.  
- http://<host:port>/api/listOpenOnly // This allows you to view only opening times.  
- http://<host:port>/api/listCloseOnly // This allows you to view only closing times.  
2.) Obtain a CSV version of the times:  
- http://<host:port>/api/listAll/csv // This allows you to obtain the CSV of opening and closing times.  
- http://<host:port>/api/listOpenOnly/csv // This allows you to obtain the CSV of only opening times.  
- http://<host:port>/api/listCloseOnly/csv // This allows you to obtain the CSV of only closing times.  
3.) Obtain a JSON version of the times:  
- http://<host:port>/api/listAll/json // This allows you to obtain the JSON of opening and closing times.  
- http://<host:port>/api/listOpenOnly/json // This allows you to obtain the JSON of opening times.  
- http://<host:port>/api/listCloseOnly/json // This allows you to obtain the JSON of closing times.  
4.) Obtain a set amount of the times from any format:  
- http://<host:port>/api/listOpenOnly/csv?top=3 // Grab only the top 3 entries of the CSV for opening and closing times.  
- http://<host:port>/api/listCloseOnly/json?top=4 // Grab only the top 4 entries of the JSON for closing times.  
- http://<host:port>/api/listAll?top=10 // Grab only the top 10 entries of opening and closing times.  
  
Don't forget to log out at the very end! Happy Biking!  
